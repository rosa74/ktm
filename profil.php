<?php
session_start();


if (isset($_GET['couleur'])) {
    setcookie('couleur', $_GET['couleur'], (time() + 3600), '');
    $_COOKIE['couleur'] = $_GET['couleur'];
} 
   

?>



<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>profil</title>
    <style>
        .casque {
            border: grey solid 2px;
            width: 20%;
            border-radius: 50%;
            box-shadow: 5px 5px 5px grey;
        }

        #profil {
            border: grey solid 1px;
            display: block;
            text-align: center;
            margin: 100px auto;
            width: 35%;
            padding: 80px;
            box-shadow: 2px 2px 2px 2px grey;

        }

        .noir {
            width: 50px;
            height: 50px;
            background-color: black;
        }

        .gris {
            width: 50px;
            height: 50px;
            background-color: grey;
        }

        .clair {
            width: 50px;
            height: 50px;
            background-color: lightgrey;
        }

        #couleur {
            display: flex;
            justify-content: space-around;
            padding: 40px;
            align-items: center;

        }

        h2 {
            font-size: 40px;
            font-weight: bold;
        }

        .modifier {
            background-color: #FF6F0F;
            border-radius: 30px;
            text-decoration: none;
            padding: 20px;
            color: black;
            box-shadow: 2px 2px 2px 2px grey;
        }

        .MDP {
            box-shadow: 2px 2px 2px 2px grey;
            margin: 0 0 40px 0;
        }

        h1 {
            margin-top: 10px;
            font-size: 40px;
            font-weight: bold;

        }

        .suppression {
            background-color: lightpink;
            color: red;
            text-transform: uppercase;
            font-weight: bold;
            padding: 10px 150px;
        }

        .supprimer {
            width: 90%;
            margin: auto;
            text-align: center;
            margin-bottom: 10px;
            padding: 20px;
        }
    </style>
</head>

<body>

    <?php
    include "nav.php";

    $nvmdp = isset($_GET['nvmdp']) && !empty($_GET['nvmdp']) ? $_GET['nvmdp'] : '';
    $submit = isset($_GET['submit']) && !empty($_GET['submit']) ? $_GET['submit'] : '';
    if ($submit) {
        $fp = fopen("formulaire/" . $_SESSION['pseudo'] . ".txt", "r+");
        $pseudo = fgets($fp);
        fwrite($fp, $nvmdp);
        fclose($fp);
    }

    $recup = isset($_GET['sup']) ? $_GET['sup'] : '';
    if (isset($_GET['sup']) && !empty($_GET['sup'])) {
        if ($recup == 'ok') {
            unlink("formulaire/" . $_SESSION['pseudo'] . ".txt");
            session_destroy();
            header('Location: connexion.php');
        }
    }


    ?>

    <div id="profil">

        <h1> <?php echo $_SESSION['pseudo']; ?> </h1>

        <img class="casque" src="img/logo/logo-profil.jpg">

        <div id="couleur">

            <h2> Thèmes </h2>

            <a class="noir" href="?couleur=black"></a>
            <a class="gris" href="?couleur=grey"></a>
            <a class="clair" href="?couleur=lightgrey"></a>

        </div>

        <form method="get">

            <input class="nvmdp" type="password" name="nvmdp" placeholder="Mot nouveau de passe"><br>
            <input type="submit" name="submit" class="modifier" value="Modifier">

        </form>

        <div class="supprimer">
            <a class="suppression" href='?sup=ok'>SUPPRIMER MON COMPTE</a>
        </div>

    </div>
</body>

</html>