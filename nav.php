<nav>

    <img src='img/logo/logo.png'>

    <ul>

        <?php

        if (isset($_SESSION["pseudo"])) {
            echo "  <a href='moto.php'>MOTO</a>
                <a href='voiture.php'>VOITURE</a>
                <a href='contact.php'>CONTACT</a>
                        
                <a class='pseudo' href='profil.php'>" . $_SESSION["pseudo"] . "</a> 

                    <a href='?sup=ok'>LOG OUT</a>";
        } else {
            echo " <a href='connexion.php'>LOGIN</a>
            <a href='inscription.php'>REGISTER</a>";
        }


        $recup = isset($_GET['sup']) ? $_GET['sup'] : "";
        if (isset($_GET['sup']) && !empty($_GET['sup'])) {

            if ($recup == 'ok') {
                '<h2> SUPPRESSION</h2>';
                session_destroy();
                header('Location: connexion.php');
            }
        }
        if (isset($_COOKIE["couleur"]) && isset($_SESSION["pseudo"])) {
            $couleurbase = $_COOKIE["couleur"];
        } else {
            $couleurbase = "black";
        }

        ?>

    </ul>
</nav>

<style>
    body {
        margin: 0;
        font-family: Arial, Helvetica, sans-serif;
    }

    nav {
        overflow: hidden;
        background-color: <?= $couleurbase ?>;

    }

    nav img {
        float: left;
        padding: 14px 16px;
        font-size: 17px;
        width: 90px;
        height: 90px;
    }

    ul {
        float: right;
        color: grey;
        padding: 14px 16px;
        font-size: 17px;
        width: 500px;
        height: 50px;
        display: flex;
    }

    ul a {
        text-decoration: none;
        color: white;
        margin: 10px;
    }

    ul a:hover {

        color: #FF6F0F;
    }

    .pseudo {
        background-color: #FF6F0F;
        border-radius: 25px;
        padding: 10px;
        display: flex;
        align-items: center;
    }

    .pseudo:hover {
        color: black;
    }
</style>