<style>
    footer {
        overflow: hidden;
        background-color: #262829;
        text-align: center;
    }

    #COPY {
        color: white;
        
    }

    footer img {
        padding: 14px 16px;
        font-size: 17px;
        width: 50px;
        height: 50px;
    }
</style>

<footer>

        <p id="COPY">
            © - ENSSOP 2020
        </p>

        <img src="img/logo/facebook.svg">
        <img src="img/logo/twitter.svg">
        <img src="img/logo/instagram.svg">
        <img src="img/logo/youtube.svg">

</footer>