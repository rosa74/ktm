<?php
session_start();
?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>voiture</title>
    <style>
        div#carrousel figure {
            position: relative;
            width: 200%;
            margin: 0;
            padding: 0;
            font-size: 0;
            left: 0;
            text-align: left;
            animation: 30s slidy infinite;

        }

        div#carrousel figure img {
            width: 50%;
            height: 800px;
            float: left;
        }

        div#carrousel {
            width: 100%;
            overflow: hidden;

        }

        @keyframes slidy {
            0% {
                left: 0%;
            }

            20% {
                left: -10%;
            }

            30% {
                left: -30%;
            }

            50% {
                left: -60%;

            }

            100% {
                left: -100%;
            }

        }

        h2 {
            text-align: center;
            font-weight: bold;
            font-size: 40px;
        }

        a.CONFIGURER {
            text-decoration: none;
            background-color: #FF6F0F;
            border-radius: 30px;
            padding: 10px;
        }

        #infos {
            margin-bottom: 40px;
            display: flex;
            justify-content: space-around;

        }

        .voiture {
            background-color: lightgrey;
            text-align: center;
            width: 20%;
            border-bottom: solid 1px grey;
            border-radius: 10px;
            padding: 30px;
            box-shadow: 5px 5px grey;

        }

        h4 {
            font-weight: bold;
            font-size: 40px;
        }
    </style>

</head>

<body>

    <?php
    include "nav.php";
    ?>

    <div id="carrousel">
        <figure>
            <img src="img/X-bow-RR.jpg" alt>
            <img src="img/X-Bow.jpg" alt>
        </figure>
    </div>

    <h2> VOITURE SPORTIVE RÉVOLUTIONNAIRE</H2>



    <div id="infos">

        <div class="voiture">

            <h4> X-Bow </h4>
            <hr>
            <p>0 à 100 : 6s</p>
            <hr>
            <p> 250 ch</p>
            <hr>
            <p>74 900 €</p>
            <a class="CONFIGURER" href="#">CONFIGURER</a>
        </div>

        <div class="voiture">

            <h4> X-Bow RR </h4>
            <hr>
            <p>0 à 100 : 3s</p>
            <hr>
            <p>450 ch</p>
            <hr>
            <p>135 000 €</p>
            <a class="CONFIGURER" href="#">CONFIGURER</a>
        </div>


    </div>



















    <?php
    include "footer.php";
    ?>
</body>

</html>