<?php
session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>inscription</title>
    <style>
    h1 { text-align: center;
    font-size: 100px;}

    form {text-align: center;

    }
    div input { margin: 10px;
    width: 600px;
    height: 50px;
    }

    .bouton{
    background-color: red;
    width: 250px;
    height: 50px;
    border-radius: 10px;}

    .connecter{
    background-color: red;
    width: 150px;
    height: 50px;
    border-radius: 30px;
    text-decoration: none;
    padding: 10px;}

    </style>
    
</head>

<body>

    <?php
    include "nav.php";

    $pseudo = isset($_GET["pseudo"]) && !empty($_GET["pseudo"]) ? $_GET['pseudo'] : '';
    $mdp = isset($_GET["mdp"]) && !empty($_GET["mdp"]) ? $_GET['mdp'] : '';
    $confirmMdp = isset($_GET["confirmMdp"]) && !empty($_GET["confirmMdp"]) ? $_GET['confirmMdp'] : '';
    $email = isset($_GET["email"]) && !empty($_GET["email"]) ? $_GET['email'] : '';


    $pseudomdp = $pseudo ."\n". $mdp;

    if (isset($_GET["submit"])){
    if ($mdp === $confirmMdp){
         $ouvert= fopen('formulaire/' .$pseudo. '.txt' ,"w" );
        fwrite($ouvert,$pseudomdp);
        fclose($ouvert);
        header('Location: connexion.php');

    }else{
        echo "Vous n'êtes pas inscrit ! ";
    }
    }
  

    ?>

    <form action="" method="get">

        <h1> INSCRIPTION </h1>

        <div class="input">

            <input type="text" name="pseudo" placeholder="Pseudo"><br>

            <input type="password" name="mdp" placeholder="Mot de passe"><br>

            <input type="password" name="confirmMdp" placeholder="Confirmez votre mot de passe"><br>

            <input type="email" name="email" placeholder="Adresse e-mail"><br>

            <input class="bouton" type="submit" name="submit" value="S'inscrire"><br>

            <p> ou </p><br>

            <a class="connecter" href="connexion.php">Se connecter</a>

        </div>
    </form>

</body>

</html>