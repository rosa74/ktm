<?php
session_start();


?>




<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>connexion</title>
    <style>
        h1 {
            text-align: center;
            font-size: 100px;
        }

        form {
            text-align: center;

        }

        div input {
            margin: 10px;
            width: 600px;
            height: 50px;
        }

        .bouton {
            background-color: red;
            width: 250px;
            height: 50px;
            border-radius: 10px;
        }

        .connecter {
            background-color: red;
            width: 150px;
            height: 50px;
            border-radius: 30px;
            text-decoration: none;
            padding: 10px;
        }
    </style>
</head>

<body>

    <?php
    include "nav.php";


$pseudo = isset($_GET["pseudo"]) && !empty($_GET["pseudo"]) ? $_GET['pseudo'] : '';
$mdp = isset($_GET["mdp"]) && !empty($_GET["mdp"]) ? $_GET['mdp'] : '';

$ouvert = $pseudo . "\n" . $mdp;  


if (is_file('formulaire/'.$pseudo .'.txt')) {
    $ouvert = fopen('formulaire/'.$pseudo .'.txt','r');
    $pseudobase = trim(fgets($ouvert));
    $mdpbase = trim(fgets($ouvert));

    if ($pseudo == $pseudobase && $mdp == $mdpbase){
        $_SESSION['pseudo'] = $pseudo;
        header('Location: moto.php');

} else {
    $error = 'Vous n\'êtes pas inscrit';
    echo $error;
}        

 }   
 

 ?>

    <form method="get">

        <h1> CONNEXION </h1>

        <div class="connexion">

            <input type="text" name="pseudo" placeholder="Pseudo"><br>

            <input type="password" name="mdp" placeholder="Mot de passe"><br>

            <p> Vous n'êtes pas inscrit </p><br>

            <input class="bouton" type="submit" value="Se connecter"><br>

            <p> ou </p><br>

            <a class="connecter" href="inscription.php">Créer un compte</a>

        </div>
    </form>

</body>

</html>