<?php
    // Infos sur les produits

    $xbow = [
        'Modele' => 'KTM X-Bow', 
        'Prix' => 74990, 
        'Option1' => 800, 
        'ImgOption1' => '/big/img/pieces/pot-voiture.jpg', 
        'Option2' => 500, 
        'ImgOption2' => '/big/img/pieces/suspension.jpg', 
        'ImgProduit' => '/big/img/KTM-X-Bow-RR5.jpg'
    ];

    $xbowrr = [
        'Modele' => 'KTM X-Bow RR', 
        'Prix' => 135000, 
        'Option1' => 1000, 
        'ImgOption1' => '/big/img/pieces/pot-voiture.jpg', 
        'Option2' => 700, 
        'ImgOption2' => '/big/img/pieces/suspension.jpg', 
        'ImgProduit' => '/big/img/X-bow-race.jpg'
    ];  

    $sx = [
        'Modele' => 'KTM 85 SX', 
        'Prix' => 5799, 
        'Option1' => 200, 
        'ImgOption1' => '/big/img/pieces/pot-moto.jpg', 
        'Option2' => 200, 
        'ImgOption2' => '/big/img/pieces/cadre.jpg', 
        'ImgProduit' => '/big/img/slide-1.jpg'
    ];
    
    $smc = [
        'Modele' => 'KTM 690 SMC R', 
        'Prix' => 11099, 
        'Option1' => 400, 
        'ImgOption1' => '/big/img/pieces/pot-moto.jpg', 
        'Option2' => 200, 
        'ImgOption2' => '/big/img/pieces/cadre.jpg', 
        'ImgProduit' => '/big/img/slide-2.jpg'
    ];

    $duke = [
        'Modele' => 'KTM 390 DUKE', 
        'Prix' => 5999, 
        'Option1' => 200, 
        'ImgOption1' => '/big/img/pieces/pot-moto.jpg', 
        'Option2' => 300, 
        'ImgOption2' => '/big/img/pieces/cadre.jpg', 
        'ImgProduit' => '/big/img/slide-3.jpg'
    ]; 

?>