<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>contact</title>

    <style>
        h1 {
            text-align: center;
            font-size: 100px;
        }

        form {
            text-align: center;

        }

        div input {
            margin: 10px;
            width: 600px;
            height: 50px;
        }

        .envoyer {
            background-color: red;
            border-radius: 30px;
            width: 600px;
            height: 50px;

        }

        .msg {background-color: green;
            color: white;
            border: solid 1px grey;
            width: 200px;
            padding:20px;
            border-radius: 10px;
            margin:0 auto;
            }

    </style>
</head>

<body>

    <?php
    include "nav.php";

$nom = isset($_GET["nom"]) && !empty($_GET["nom"]) ? $_GET['nom'] : '';
$prenom = isset($_GET["prenom"]) && !empty($_GET["prenom"]) ? $_GET['prenom'] : '';
$email = isset($_GET["email"]) && !empty($_GET["email"]) ? $_GET['email'] : '';
$message = isset($_GET["message"]) && !empty($_GET["message"]) ? $_GET['message'] : '';
$merci = null;

$tout = $nom . "\n" . $prenom . "\n" .$email. "\n" .$message;

if (isset($_GET["submit"])) {
    
        $ouvert = fopen('contact/' . $nom . '.txt', "w");
        
        fwrite($ouvert, $tout);
        fclose($ouvert);

        $merci = "<p class='msg'> Merci de votre message ! </p>"; 
}

   
    ?>

    <form method="get">

        <h1> CONTACT </h1>

        <div class="input">

            <input type="text" name="nom" placeholder="Nom"><br>

            <input type="text" name="prenom" placeholder="Prenom"><br>

            <input type="email" name="email" placeholder="Adresse e-mail"><br>

            <input type="text" name="message" placeholder="Votre message"><br><br><br>

                                <?php echo $merci;?>

            <input class="envoyer" type="submit" name="submit" value="Envoyer">

        </div>
    </form>

</body>

</html>