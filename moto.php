<?php
session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>moto</title>
    <style>
        .modeles {
            width: 75%;
            border: 1px black solid;
            display: flex;
            margin: 2% 12.5%;
            align-items: center;
        }

        img {
            width: 30%;

        }

        h1 {
            text-align: center;
            font-size: 80px;
        }

        div a.CONFIGURER {
            text-decoration: none;
            background-color: #FF6F0F;
            border-radius: 30px;
            padding: 20px;
        }

        .text {
            width: 50%;
        }

        h4 {
            font-size: 30px;
        }
    </style>
</head>

<body>
    <?php
    include "nav.php";
    ?>


    <h1>SUPPREMACY POWER ! </h1>

    <div class="moto">

        <div class="modeles">
            <img src="img/sx-1.jpg">

            <div class="text">
                <h4>85 SX - 5 799 € </h4>
                <p>Logé dans un cadre léger de qualité supérieure, le moteur compact offre un niveau de performance éblouissant et
                    promet de belles sensations aux champions en herbe.
                    Cette petite baroudeuse est l’arme ultime des jeunes pousses prêtes à en
                    découdre sur la piste et à lutter pour le podium.</p>
            </div>

            <div><a class="CONFIGURER" href="#">CONFIGURER</a></div>

        </div>

        <div class="modeles">
            <img src="img/smc-2.jpg">

            <div class="text">
                <h4>690 SMC R - 11 099 € </h4>
                <p>Logé dans un cadre léger de qualité supérieure, le moteur compact offre un niveau de performance éblouissant et
                    promet de belles sensations aux champions en herbe.
                    Cette petite baroudeuse est l’arme ultime des jeunes pousses prêtes à en
                    découdre sur la piste et à lutter pour le podium.</p>
            </div>

            <div><a class="CONFIGURER" href="#">CONFIGURER</a></div>

        </div>

        <div class="modeles">
            <img src="img/duke-3.jpg">

            <div class="text">
                <h4>390 DUKE - 5 999 € </h4>
                <p>Logé dans un cadre léger de qualité supérieure, le moteur compact offre un niveau de performance éblouissant et
                    promet de belles sensations aux champions en herbe.
                    Cette petite baroudeuse est l’arme ultime des jeunes pousses prêtes à en
                    découdre sur la piste et à lutter pour le podium.</p>
            </div>

            <div><a class="CONFIGURER" href="#">CONFIGURER</a></div>

        </div>

    </div>

    <?php
    include "footer.php";
    ?>

</body>

</html>